Edgar Sioson
2013-01-05

This Bitbucket ("Bb") repository was created as a practice shared DVCS repository, 
as explained in the first example in 
http://git-scm.com/book/en/Distributed-Git-Distributed-Workflows.


You could practice:
- Cloning: copy the remote Bb repo to your local machine, development 
environment, or personal workspace

- Forking: create another repo on the remote Bb repo which you 
then clone

- Pulling: Fetch and merge changes from remote to your clone (default merge is not
recommended -- see rebase option instead)

- Rebase: use this to present a cleaner commit history, see
http://gitready.com/advanced/2009/02/11/pull-with-rebase.html

- Branching: highly recommended to create a separate branch for each feature or 
bug fix, which would only be rebased to the master branch once stable

- Squash commits: In interactive rebase, you could squash the commits to condense or 
reorganize your commits in a more logical manner

- Ignore settings: For example, I exclude lib/*, data/* and misc/* folders. 

- And many other git repository interactions. 

I use msysgit and the Git GUI a lot for common commands.


/***********************************************************************************
THIS REPO IS -NOT- A RECOMMENDATION TO USE A SHARED TEAM REPO FOR AN IHME PROJECT.
***********************************************************************************/  
	
- My experience within IHME indicates only low, infrequent code-level collaboration. 
Instead of setting up a shared repo, I recommend one of the following workflows.

- Clone-Patch (for rare code contributions). Simply clone the remote repo to your 
development environment (you don't have to create a Bb account or repo), create a branch 
for the feature or bug fix you want to code, then when you are done create a 
patch using git. Email the maintainer the patch. See
https://ariejan.net/2009/10/26/how-to-create-and-apply-a-patch-with-git.

- Fork-Pull (for infrequent to regular code contributions): Fork the remote repo and ask 
the maintainer to pull stable changes from your repo. In more detail, create a new branch for 
each feature or bug fix, commit locally/push the branch to your origin as often as you like, 
always pull/rebase on the maintainer's master branch while your developing to 
minimize painful merges, squash commits-rebase when branch is stable, then send the 
maintainer a pull request.
